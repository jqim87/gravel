Attendees:
James Maclean
Tejeswara Reddy
Saratchandra Pappu
Vijay Srinivas

Go though a working example of the build system

Tech stack overview:
Build system -> Gulp, npm
HTML -> Nunjucks
CSS -> Sass, Foundation
JS -> Browserfy, Foundation, jQuery
Output/Deliverables:
Blocks of HTML
main.css (all css)
JS CDN links (Libraries and frameworks)
main.js (custom scripts)
Code styles:
CSS/SCSS = OOCSS
HTML = try to keep it common

Q&A

NPM + Node.js is a requirement

src = uncompiled source
dev = Working enviroment, quick compile
dist = fully compiled and minified production code
 * HTML blocks -> Frontify
 * compiles main.min.js
 * compiled main.min.css
 * Assets compressed (images, font files)
