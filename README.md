# pebbles
##Installation
NOTE: It is useful to have gulp installed  globally
1. Clone the project
2. run $npm install
3. run $gulp serve
This will fill the 'dev' folder with assets and code and then run a local server

##Publishing
run $gulp build
This will fill the 'dist' folder with optimized assets and code

### CSS
General rules:
1. Use the Foundation framework before writing more CSS
2. Class style and structure is OOCSS, which follows the Foundation framework style
3. Idiomatic CSS for commending style
4. All values input with named variables
5. Any 'magic numbers' must be explained in comments
