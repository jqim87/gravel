var $singlePickers = $('.js-dateRangePicker-single');
var $rangePickers = $('.js-dateRangePicker-range');
var $inlinePicker = $('.js-dateRangePicker-inline');
var $inlinePickerContainer = $('.js-dateRangePicker-container');
function init() {
	if ($singlePickers.length) {
		$singlePickers.dateRangePicker({
			autoClose: true,
			singleDate : true,
			showShortcuts: false,
			singleMonth: true,
			startOfWeek: 'monday',
			showShortcuts: false,
			showTopbar: false,
			hoveringTooltip: false,
			customArrowPrevSymbol: ' ',
			customArrowNextSymbol: ' '
		});
	};

	if ($rangePickers.length) {
		$rangePickers.dateRangePicker({
			autoClose: true,
			showShortcuts: false,
			startOfWeek: 'monday',
			showShortcuts: false,
			showTopbar: false,
			hoveringTooltip: false,
			customArrowPrevSymbol: ' ',
			customArrowNextSymbol: ' '
		});
	};

	if ($inlinePicker.length && $inlinePickerContainer.length) {
		$inlinePicker.dateRangePicker({
			singleDate : true,
			showShortcuts: false,
			singleMonth: true,
			startOfWeek: 'monday',
			showShortcuts: false,
			showTopbar: false,
			hoveringTooltip: false,
			customArrowPrevSymbol: ' ',
			customArrowNextSymbol: ' ',
			inline:true,
			container: $inlinePickerContainer,
			alwaysOpen:true
		});
	};

};
init();
