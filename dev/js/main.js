(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
var clickHandler = null;
var clickHandlerLinked = null;
var size = null;
var $allControls = null;
var $allPanels = null;

function calculateClick() {

    if (Foundation.MediaQuery.atLeast(size)) {
        if ($allPanels.hasClass('is-open')) {
            $allControls.addClass('is-open');
            $allPanels.addClass('is-open');
        }
        clickHandlerLinked = function () {
            $allControls.toggleClass('is-open');
            $allPanels.toggleClass('is-open');
        };
    } else {
        clickHandlerLinked = function ($thisControl, $thisPanel) {
            $thisControl.toggleClass('is-open');
            $thisPanel.toggleClass('is-open');
        };
    }
}

function initLinked($el, setSize) {

    if (setSize !== undefined) {
        size = setSize;
        $allControls = $el.find('.show-hide-control');
        $allPanels = $el.find('.show-hide-panel');
    } else {
        console.error('no breakpoint specified for initLinked()');
    }

    $el.each(function () {
        var $control = $(this).find('.show-hide-control');
        var $panel = $(this).find('.show-hide-panel');

        $control.on('click', function () {
            clickHandlerLinked($control, $panel);
        });
    });

    calculateClick();

    //https://css-tricks.com/snippets/jquery/done-resizing-event/
    var resizeTimer;
    $(window).on('resize', function (e) {

        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {

            // Run code here, resizing has "stopped"
            calculateClick();
        }, 250);
    });
};

function initSeparate($el) {

    clickHandler = function ($thisControl, $thisPanel) {
        $thisControl.toggleClass('is-open');
        $thisPanel.toggleClass('is-open');
    };

    $el.each(function () {
        var $control = $(this).find('.show-hide-control');
        var $panel = $(this).find('.show-hide-panel');

        $control.on('click', function () {
            clickHandler($control, $panel);
        });
    });
};
// initLinked($('.show-hide-wrap-linked'), 'large');
// initSeparate($('.show-hide-wrap'));
exports.initSeparate = initSeparate;
exports.initLinked = initLinked;

},{}],2:[function(require,module,exports){
function init($el, $clearOf) {

    var topVal = $clearOf.outerHeight();

    function makeSticky() {
        if (Foundation.MediaQuery.is('large')) {
            $el.$sticky = new Foundation.Sticky($el, {
                stickyOn: 'medium',
                stickTo: 'bottom',
                marginBottom: '0'
            });
        } else {
            $el.$sticky = new Foundation.Sticky($el, {
                stickyOn: 'small',
                topAnchor: topVal,
                marginTop: '0'
            });
        }
    };
    makeSticky();

    var resizeTimer;
    $(window).on('resize', function (e) {

        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {

            // Run code here, resizing has "stopped"
            $el.foundation('_destroy');
            makeSticky();
        }, 500);
    });
};

//https://css-tricks.com/snippets/jquery/done-resizing-event/


exports.init = init;

},{}],3:[function(require,module,exports){
var $singlePickers = $('.js-dateRangePicker-single');
var $rangePickers = $('.js-dateRangePicker-range');
var $inlinePicker = $('.js-dateRangePicker-inline');
var $inlinePickerContainer = $('.js-dateRangePicker-container');
function init() {
	if ($singlePickers.length) {
		$singlePickers.dateRangePicker({
			autoClose: true,
			singleDate: true,
			showShortcuts: false,
			singleMonth: true,
			startOfWeek: 'monday',
			showShortcuts: false,
			showTopbar: false,
			hoveringTooltip: false,
			customArrowPrevSymbol: ' ',
			customArrowNextSymbol: ' '
		});
	};

	if ($rangePickers.length) {
		$rangePickers.dateRangePicker({
			autoClose: true,
			showShortcuts: false,
			startOfWeek: 'monday',
			showShortcuts: false,
			showTopbar: false,
			hoveringTooltip: false,
			customArrowPrevSymbol: ' ',
			customArrowNextSymbol: ' '
		});
	};

	if ($inlinePicker.length && $inlinePickerContainer.length) {
		$inlinePicker.dateRangePicker({
			singleDate: true,
			showShortcuts: false,
			singleMonth: true,
			startOfWeek: 'monday',
			showShortcuts: false,
			showTopbar: false,
			hoveringTooltip: false,
			customArrowPrevSymbol: ' ',
			customArrowNextSymbol: ' ',
			inline: true,
			container: $inlinePickerContainer,
			alwaysOpen: true
		});
	};
};
init();

},{}],4:[function(require,module,exports){

var $steppers = $(".stepper-wrap, .range-selector-wrap");

function makeSteppers(el) {

    el.each(function () {
        var $stepUp = $(this).find("button[name='step-up']");
        var $stepDown = $(this).find("button[name='step-down']");
        var $input = $(this).find("input[type='number']");
        var min = $input.attr('min');
        var max = $input.attr('max');
        var step = parseFloat($input.attr('step'));

        $stepUp.on('click', function () {
            handelStep('up');
        });
        $stepDown.on('click', function () {
            handelStep('down');
        });

        function handelStep(direction) {
            var oldVal = parseFloat($input.val());

            if (oldVal < min) {
                $input.val(min);
            } else if (oldVal > max) {
                $input.val(max);
            } else if (direction == 'up' && oldVal < max) {
                var newVal = oldVal + step;
                $input.val(newVal);
            } else if (direction == 'down' && oldVal > min) {
                var newVal = oldVal - step;
                $input.val(newVal);
            }
        };
    });
};
makeSteppers($steppers);

},{}],5:[function(require,module,exports){

var $passwordFeilds = $(".password-wrap");

function makeShowPass(el) {
    el.each(function () {

        var $showPassBtn = $(this).find("button[name='show-pass']");
        var $passInput = $(this).find("input[type='password']");

        $showPassBtn.on('click', function () {
            if ($passInput.hasClass('is-show')) {
                $passInput.attr('type', 'password');
                $passInput.removeClass('is-show');
            } else {
                $passInput.attr('type', 'text');
                $passInput.addClass('is-show');
            }
        });
    });
};
makeShowPass($passwordFeilds);

},{}],6:[function(require,module,exports){
$(document).foundation();
require('./elements/number-input.js');
require('./elements/text-input.js');
require('./elements/date-picker.js');
// require('./components/show-hide.js');
var showHide = require('./components/show-hide');
var stickyTotal = require('./components/sticky-total.js');

$(document).ready(function () {

    showHide.initLinked($('.show-hide-wrap-linked'), 'large');
    showHide.initSeparate($('.show-hide-wrap'));
    stickyTotal.init($('#stickyTotal'), $('.topnav'));
});

},{"./components/show-hide":1,"./components/sticky-total.js":2,"./elements/date-picker.js":3,"./elements/number-input.js":4,"./elements/text-input.js":5}]},{},[6]);
